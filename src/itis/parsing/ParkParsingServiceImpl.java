package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;
import javafx.util.converter.LocalDateStringConverter;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {

        FileReader fileReader = new FileReader(parkDatafilePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        Class park = Class.forName("itis.parsing.Park");
        Constructor constructor = park.getDeclaredConstructor();
        constructor.setAccessible(true);
        Park parkImpl = (Park) constructor.newInstance();
        ArrayList<String> readData = new ArrayList<>();
        ArrayList<ParkParsingException.ParkValidationError> errors = new ArrayList<>();
        String str;
        while ((str = bufferedReader.readLine()) != null) {
            if (!str.equals("***"))
                readData.add(str);
        }
        bufferedReader.close();
        for (String line : readData) {
            String[] data = line.split(":");
            String fieldName = data[0].replace('"', ' ');
            String fieldData = data[1].replace('"', ' ');
            fieldName = fieldName.replaceAll(" ", "");
            fieldData = fieldData.replaceAll(" ", "");
            try {
                Field field = park.getDeclaredField(fieldName);
                field.setAccessible(true);
                if (field.getName().equals("foundationYear")) {
                    if (!fieldData.equals("")) {
                        LocalDate localDate = readDateAndTime(fieldData);
                        field.set(parkImpl, localDate);
                    }
                } else
                    field.set(parkImpl, fieldData);
                processAnnotation(field, parkImpl, readData, errors);
            } catch (NoSuchFieldException e) {
            }
        }
                if (errors.size()==0) {
                    return parkImpl;
                } else {
                    throw new ParkParsingException("Ошибка парсинга парка", errors);
                }
    }


    private void processAnnotation(Field field, Park parkImp, ArrayList<String> readData, ArrayList<ParkParsingException.ParkValidationError> errors) throws IllegalAccessException {
        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotationFromList : annotations) {
            if (annotationFromList instanceof FieldName) {
                FieldName fieldName = (FieldName) annotationFromList;
                String name = ((FieldName) annotationFromList).value();
                for (String stringFromList : readData) {
                    String[] data = stringFromList.split(":");
                    String fieldName1 = data[0].replace('"', ' ');
                    fieldName1 = fieldName1.replaceAll(" ", "");
                    if (fieldName1.equals(name)) {
                        String fieldData1 = data[1].replace('"', ' ');
                        fieldData1 = fieldData1.replaceAll(" ", "");
                        field.set(parkImp, fieldData1);
                    }
                }
            }
            if (annotationFromList instanceof MaxLength) {
                MaxLength maxLength = (MaxLength) annotationFromList;
                int digit = maxLength.value();
                if (((String) field.get(parkImp)).length() > 13) {
                    ParkParsingException.ParkValidationError error = new ParkParsingException.ParkValidationError(field.getName(), "Значение должно быть меньше");
                    errors.add(error);
                }
            }
            if (annotationFromList instanceof NotBlank) {
                if (field.get(parkImp ) == null) {
                    ParkParsingException.ParkValidationError error = new ParkParsingException.ParkValidationError(field.getName(), "Значение поля не может быть null");
                    errors.add(error);
                } else {
                    if (field.getType().getName().equals("java.time.LocalDate")) {
                        if (field.get(parkImp) == null) {
                            ParkParsingException.ParkValidationError error = new ParkParsingException.ParkValidationError(field.getName(), "Пустая дата");
                            errors.add(error);
                        }
                    } else {
                        String value = (String) field.get(parkImp);
                        if (value.equals(" ")) {
                            ParkParsingException.ParkValidationError error = new ParkParsingException.ParkValidationError(field.getName(), "Значение поля не может быть пустым");
                            errors.add(error);
                        }
                    }
                }
            }
        }
    }

    private LocalDate readDateAndTime(String data) {
        return LocalDate.parse(data);
    }

}
