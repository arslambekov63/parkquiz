package itis.parsing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

interface ParkParsingService {

    Park parseParkData(String parkDatafilePath) throws ParkParsingException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException;

}
